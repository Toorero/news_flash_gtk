use crate::content_page::ArticleListMode;
use crate::sidebar::models::SidebarSelection;
use news_flash::models::ArticleID;
use std::collections::HashSet;

#[derive(Clone, Debug)]
pub struct ContentPageState {
    sidebar_selection: SidebarSelection,
    article_list_mode: ArticleListMode,
    search_term: Option<String>,
    offline: bool,
    prefer_scraped_content: bool,
    ongoing_scraping_articles: HashSet<ArticleID>,
}

const ARTICLE_LIST_PAGE_SIZE: i64 = 20;

impl ContentPageState {
    pub fn new() -> Self {
        ContentPageState {
            sidebar_selection: SidebarSelection::All,
            article_list_mode: ArticleListMode::All,
            search_term: None,
            offline: false,
            prefer_scraped_content: false,
            ongoing_scraping_articles: HashSet::new(),
        }
    }

    pub fn page_size() -> i64 {
        ARTICLE_LIST_PAGE_SIZE
    }

    pub fn get_sidebar_selection(&self) -> &SidebarSelection {
        &self.sidebar_selection
    }

    pub fn set_sidebar_selection(&mut self, selection: SidebarSelection) {
        self.sidebar_selection = selection;
    }

    pub fn get_article_list_mode(&self) -> &ArticleListMode {
        &self.article_list_mode
    }

    pub fn set_article_list_mode(&mut self, header: ArticleListMode) {
        self.article_list_mode = header;
    }

    pub fn get_search_term(&self) -> &Option<String> {
        &self.search_term
    }

    pub fn set_search_term(&mut self, search_term: Option<String>) {
        self.search_term = search_term;
    }

    pub fn set_offline(&mut self, offline: bool) {
        self.offline = offline;
    }

    pub fn get_offline(&self) -> bool {
        self.offline
    }

    pub fn set_prefer_scraped_content(&mut self, pref: bool) {
        self.prefer_scraped_content = pref;
    }

    pub fn get_prefer_scraped_content(&self) -> bool {
        self.prefer_scraped_content
    }

    pub fn started_scraping_article(&mut self, id: &ArticleID) {
        self.ongoing_scraping_articles.insert(id.clone());
    }

    pub fn finished_scraping_article(&mut self, id: &ArticleID) {
        self.ongoing_scraping_articles.remove(id);
    }

    pub fn is_article_scrap_ongoing(&self, id: &ArticleID) -> bool {
        self.ongoing_scraping_articles.contains(id)
    }
}

impl PartialEq for ContentPageState {
    fn eq(&self, other: &ContentPageState) -> bool {
        if self.sidebar_selection != other.sidebar_selection {
            return false;
        }
        if self.article_list_mode != other.article_list_mode {
            return false;
        }
        match &self.search_term {
            Some(self_search_term) => match &other.search_term {
                Some(other_search_term) => {
                    if self_search_term != other_search_term {
                        return false;
                    }
                }
                None => return false,
            },
            None => match &other.search_term {
                Some(_) => return false,
                None => {}
            },
        }
        true
    }
}
