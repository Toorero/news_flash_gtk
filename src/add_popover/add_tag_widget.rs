use crate::app::Action;
use crate::color::ColorRGBA;
use crate::util::Util;
use gdk4::RGBA;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, ColorButton, CompositeTemplate, Entry, Widget};

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_tag_widget.ui")]
    pub struct AddTagWidget {
        #[template_child]
        pub add_tag_button: TemplateChild<Button>,
        #[template_child]
        pub tag_entry: TemplateChild<Entry>,
        #[template_child]
        pub color_button: TemplateChild<ColorButton>,
    }

    impl Default for AddTagWidget {
        fn default() -> Self {
            Self {
                add_tag_button: TemplateChild::default(),
                tag_entry: TemplateChild::default(),
                color_button: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddTagWidget {
        const NAME: &'static str = "AddTagWidget";
        type ParentType = Box;
        type Type = super::AddTagWidget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddTagWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("tag-added", &[], <()>::static_type().into()).build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self, widget: &Self::Type) {
            widget.init();
        }
    }

    impl WidgetImpl for AddTagWidget {}

    impl BoxImpl for AddTagWidget {}
}

glib::wrapper! {
    pub struct AddTagWidget(ObjectSubclass<imp::AddTagWidget>)
        @extends Widget, Box;
}

impl AddTagWidget {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    fn init(&self) {
        let imp = imp::AddTagWidget::from_instance(self);

        let tag_add_button = imp.add_tag_button.get();
        let tag_entry = imp.tag_entry.get();
        let color_button = imp.color_button.get();

        // make parse button sensitive if entry contains text and vice versa
        imp.tag_entry
            .connect_changed(clone!(@weak tag_add_button => @default-panic, move |entry| {
                tag_add_button.set_sensitive(!entry.text().as_str().is_empty());
            }));

        // hit enter in entry to add tag
        imp.tag_entry
            .connect_activate(clone!(@weak tag_add_button => @default-panic, move |_entry| {
                if tag_add_button.get_sensitive() {
                    tag_add_button.emit_clicked();
                }
            }));

        imp.add_tag_button.connect_clicked(clone!(
            @weak self as widget,
            @weak tag_entry,
            @weak color_button => @default-panic, move |_button|
        {
            let rgba = color_button.rgba();
            let rgba = ColorRGBA::from_normalized(rgba.red() as f64, rgba.green() as f64, rgba.blue() as f64, rgba.alpha() as f64);
            let color = rgba.to_string_no_alpha();
            if !tag_entry.text().as_str().is_empty() {
                Util::send(Action::AddTag(color, tag_entry.text().as_str().into()));
                widget.emit_by_name::<()>("tag-added", &[]);
            }
        }));
    }

    pub fn reset(&self) {
        let imp = imp::AddTagWidget::from_instance(self);

        imp.tag_entry.set_text("");
        imp.color_button
            .set_rgba(&RGBA::new(115.0 / 255.0, 210.0 / 255.0, 22.0 / 255.0, 1.0));
    }
}
