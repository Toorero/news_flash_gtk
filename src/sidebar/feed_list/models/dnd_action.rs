use super::FeedListItemID;
use news_flash::models::CategoryID;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct FeedListDndAction {
    pub item_id: Arc<FeedListItemID>,
    pub new_parent: CategoryID,
    pub sort_index: i32,
}
